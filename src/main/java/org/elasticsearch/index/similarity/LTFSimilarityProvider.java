/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.elasticsearch.index.similarity;

import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.inject.assistedinject.Assisted;
import org.elasticsearch.common.settings.Settings;

/**
 * Similarity Provider that is based on the default similarity, however ignores the term frequency of the found term.
 * (LTF = Limit Term Frequency).
 *
 * @author Darran Kartaschew
 */
public class LTFSimilarityProvider extends AbstractSimilarityProvider {

    /**
     * Our similarity engine.
     */
    private LTFSimilarity similarity;

    @Inject
    public LTFSimilarityProvider(@Assisted String name, @Assisted Settings settings) {
        super(name);
        this.similarity = new LTFSimilarity();
        boolean discountOverlaps = settings.getAsBoolean("discount_overlaps", true);
        this.similarity.setDiscountOverlaps(discountOverlaps);
    }

    @Override
    public LTFSimilarity get() {
        return similarity;
    }
}
