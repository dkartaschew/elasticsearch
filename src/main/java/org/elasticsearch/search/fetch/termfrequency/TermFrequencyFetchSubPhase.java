/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.elasticsearch.search.fetch.termfrequency;

import com.google.common.collect.ImmutableMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;

import org.elasticsearch.ElasticSearchException;
import org.elasticsearch.search.SearchParseElement;
import org.elasticsearch.search.fetch.FetchPhaseExecutionException;
import org.elasticsearch.search.fetch.FetchSubPhase;
import org.elasticsearch.search.internal.InternalSearchHit;
import org.elasticsearch.search.internal.SearchContext;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.lucene.index.DocsAndPositionsEnum;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.util.BytesRef;
import org.elasticsearch.search.fetch.terms.TermVector;

/**
 * @author Darran Kartaschew
 */
public class TermFrequencyFetchSubPhase implements FetchSubPhase {

    @Override
    public Map<String, ? extends SearchParseElement> parseElements() {
        return ImmutableMap.of("term_frequency", new TermFrequencyParseElement());
    }

    @Override
    public boolean hitsExecutionNeeded(SearchContext context) {
        return false;
    }

    @Override
    public void hitsExecute(SearchContext context, InternalSearchHit[] hits) throws ElasticSearchException {
    }

    @Override
    public boolean hitExecutionNeeded(SearchContext context) {
        return (context.termFrequency() != null);
    }

    @Override
    public void hitExecute(SearchContext context, HitContext hitContext) throws ElasticSearchException {
        try {
            List<TermFrequency> listTermFrequencies = new ArrayList<TermFrequency>();
            listTermFrequencies.add(new TermFrequency());

            /*
             * Get our original terms, as Lucene only give you all term vectors for a given document 
             * (and we have to manually match our own search terms with the terms Lucene gives us. (very, very sucky).
             */
            Set<Term> termCollection = new LinkedHashSet<Term>();
            context.query().extractTerms(termCollection);

            // we use the top level doc id, since we work with the top level searcher
            if (hitContext.reader() != null) {
                Terms termVector;
                try {
                    termVector = hitContext.reader().getTermVector(hitContext.hit().docId(), "content");
                } catch (IndexOutOfBoundsException ex) {
                    termVector = null; // just set the tem vector to null, and exit.
                }
                if (termVector == null) {
                    hitContext.hit().termFrequency(listTermFrequencies);
                    return;
                }

                TermsEnum luceneTermsEnum = termVector.iterator(null);
                BytesRef ref;
                DocsAndPositionsEnum docsAndPositions = null;
                listTermFrequencies.clear();

                // Depending on the value of context.termFrequency(), then we return all, none or some.

                String setting = context.termFrequency();

                if (setting.equals("terms")) {
                    // Return just the frequencies for the terms
                    // Cycle through all terms in our query, and seek to the required 
                    Iterator<Term> terms = termCollection.iterator();
                    while (terms.hasNext()) {
                        Term term = terms.next();
                        if (luceneTermsEnum.seekExact(term.bytes(), true)) {
                            docsAndPositions = luceneTermsEnum.docsAndPositions(null, docsAndPositions);
                            // beware that docsAndPositions will be null if you didn't index positions
                            if (docsAndPositions.nextDoc() != 0) {
                                throw new AssertionError();
                            }
                            listTermFrequencies.add(new TermFrequency(term.text(), (long) docsAndPositions.freq()));
                        } else {
                            // Not found?
                            listTermFrequencies.add(new TermFrequency(term.text(), -1l));
                        }
                    }
                } else {
                    // return all terms of a minimum amount.
                    int minimumFreq = 0;

                    try {
                        minimumFreq = Integer.parseInt(setting);
                    } catch (NumberFormatException ex) {
                        // kill it, and return ALL terms and their frequencies
                    }
                    // Cycle through all terms that Lucene has.
                    while ((ref = luceneTermsEnum.next()) != null) {
                        docsAndPositions = luceneTermsEnum.docsAndPositions(null, docsAndPositions);
                        // beware that docsAndPositions will be null if you didn't index positions
                        if (docsAndPositions.nextDoc() != 0) {
                            throw new AssertionError();
                        }

                        final int freq = docsAndPositions.freq(); // number of occurrences of the term
                        if (freq >= minimumFreq) {
                            listTermFrequencies.add(new TermFrequency(new String(ref.bytes).trim(), (long) freq));
                        }
                    }
                }
            }
            // Set our terms and their frequencies
            hitContext.hit().termFrequency(listTermFrequencies);
        } catch (Exception e) {
            throw new FetchPhaseExecutionException(context, "Failed to get terms for doc [" + hitContext.hit().type() + "#" + hitContext.hit().id() + "]", e);
        }
    }
}
